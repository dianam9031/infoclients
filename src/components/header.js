import React from 'react';
import {Link} from 'react-router-dom';

const Header = () => {
    return (
      <nav className='navbar navbar-expand-lg navbar-light bg-light'>
         <Link className='navbar-brand' to={'/'}>Info Clients</Link>
        <button className='navbar-toggler' type='button' data-toggle='collapse' data-target='#navbarText' aria-controls='navbarText' aria-expanded='false' aria-label='Toggle navigation'>
          <span className='navbar-toggler-icon'></span>
        </button>
        <div className='collapse navbar-collapse' id='navbarText'>
            <ul className='nav navbar-nav'>
                <li className='nav-item'>
                  <Link to={'/'} className='nav-link'>Home</Link>
                </li>
                <li className='nav-item'>
                  <Link to={'/client'} className='nav-link'>New Client</Link>
                </li>
                <li className='nav-item'>
                  <Link to={'/reports'} className='nav-link'>Reports</Link>
                </li>
            </ul>
        </div>
      </nav>
    );
};

export default Header