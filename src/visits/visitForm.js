import React from 'react';
import axios from 'axios';


class VisitForm extends React.Component {

  state = { date: '', net: 0, description:'',clientId: this.props.match.params.id, salesRepId: 2};
  handleSubmit = async (event) => {
    
    const visit = {
      date: this.state.date, 
      net: this.state.net,
      description: this.state.description,
      clientId: this.props.match.params.id,
      salesRepId: 2
    };
    event.preventDefault();
    console.log("Esto es visits "+JSON.stringify(visit));
    const resp = await axios.post(`http://localhost:8080/visit`, visit);
    console.log("Visits data "+JSON.stringify(resp.data));
    this.setState({ date: '', net: 0, description:'',clientId: this.props.match.params.id, salesRepId: 2});
  };
  render() {
    return(
      <form onSubmit={this.handleSubmit}>
        <div className="form-group">
        <label htmlFor="date">Date</label>
          <input type="text" className="form-control" id="date" placeholder="2019-09-15" value={this.state.date} onChange={event => this.setState({ date: event.target.value })} />
      </div>
      <div className="form-group">
        <label htmlFor="net">Net</label>
          <input type="number" className="form-control" id="net" placeholder="10" value={this.state.net} onChange={event => this.setState({ net: event.target.value })}   />
      </div>
      <div className="form-group">
        <label htmlFor="description">description</label>
          <textarea className="form-control" id="description" placeholder="Enter description here..." value={this.state.description} onChange={event => this.setState({ description: event.target.value })}  />
      </div>
      
      <button className="btn btn-primary">Add visit</button>
      </form>
    );
  }
}

export default VisitForm