// src/App.js

    import React, { Component } from 'react';
    import {BrowserRouter as Router, Switch, Route} from 'react-router-dom';
    import ClientList from './clients/clientList';
    import ClientForm from './clients/clientForm';
    import EditForm from './clients/editForm';
    import Header from './components/header';
    import VisitForm from './visits/visitForm';
    import Report from './reports/report';
    import ClientCreditHistory from './reports/clientCreditHistory';
  

    class App extends Component {
      render() {
        return (
            <Router>
              <Header />
              <Switch>
                <Route path="/" exact component={ClientList} />
                <Route path="/client" component={ClientForm}/>
                <Route path="/visit/new/:id" component={VisitForm}/>
                <Route path="/edit/:id" component={EditForm}/>
                <Route path="/credit-history/:id" component={ClientCreditHistory}/>
                <Route path="/reports" component={Report}/>
              </Switch>
            </Router>
        )
      }
    }

    export default App