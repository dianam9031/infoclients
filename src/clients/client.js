import React from 'react';
import {Link} from 'react-router-dom';


class Client extends React.Component {
  render() {
    const client = this.props;
    return (
      <tr>
        <th scope="row">
          <Link to={`/credit-history/${client.id}`} className='nav-link'>{client.nit}</Link>
          
        </th>
          <td>{client.fullName}</td>
          <td>{client.phone}</td>
          <td>{client.address}</td>
          <td>{client.availableCredit}</td>
          <td>{client.visitsPercentage}</td>
          <td>{client.creditLimit}</td>
          <td>
            <Link className="btn btn-primary" to={`/edit/${client.id}`}>
              Edit 2
            </Link>
            <button onClick={() => this.props.deleteClient(client.id)} className="btn btn-danger">
              Delete
            </button>
          </td>
          <td><Link to={`/visit/new/${client.id}`} className='nav-link'>Add Visit</Link></td>
      </tr>
      
    );
  }
}

export default Client

