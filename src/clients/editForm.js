import React from 'react';
import axios from 'axios';


class EditForm extends React.Component {  

  
  
  state = { nit: '', fullName: '', phone:'', address:'',creditLimit:0, visitsPercentage:0, 
    cityId: 0,
    stateId: 0,
    countryId: 0,
   countries:[],states:[],cities:[]};

  handleSubmit = async (event) => {
  	const client = {
      nit: this.state.nit, 
      fullName: this.state.fullName,
      phone: this.state.phone,
      address: this.state.address,
      creditLimit: this.state.creditLimit,
      
      visitsPercentage: this.state.visitsPercentage,
      cityId: this.state.cityId
    };
    console.log("ESTO ES EL CLIENT CONST: "+client);
    event.preventDefault();
    
    const resp = await axios.post(`http://localhost:8080/client`, client);
    console.log("Client data "+JSON.stringify(resp.data));
    this.setState({ nit: '', fullName: '', phone:'', address:'',creditLimit:0, visitsPercentage:0, cityId: 0, states:[],cities:[]});
  };

  countryChangeHandler = async (event) => {
    let nam = event.target.name;
    let val = event.target.value;
    
    console.log("country:", val);
    //Pull states from API
    const resp = await axios.get(`http://localhost:8080/states/${val}`);

    this.setState({states: resp.data, countryId:val});
  }

  stateChangeHandler = async (event) => {
    let nam = event.target.name;
    let val = event.target.value;

    console.log("state:", val);
    //Pull states from API
    const resp = await axios.get(`http://localhost:8080/cities/${val}`);

    this.setState({cities: resp.data, stateId:val});
  }

  getSelectOptions(arr){
    let options = arr.map((item, i) => {
        return (
          <option key={i} value={item.id}>{item.name}</option>
        )
      }, this);
    return options;
  }

  async componentDidMount() {
    const resp = await axios.get(`http://localhost:8080/client/${this.props.match.params.id}`);
    const response = resp.data;
    this.setState({
      nit: response.nit,
      fullName: response.fullName,
      phone: response.phone,
      address: response.address,
      creditLimit: response.creditLimit,
      visitsPercentage: response.visitsPercentage,
      cityId: response.cityId
    });
  }

  render() {
  	
  	return(
      <div>
        <h1>Edit</h1>
        <form onSubmit={this.handleSubmit}>
          <div className="form-group">
             <label htmlFor="country">Country</label>
             <select value={this.state.countryId} onChange={this.countryChangeHandler} className="form-control">
                <option>Select country</option>
                {this.getSelectOptions(this.state.countries)}
             </select>
          </div>

          <div className="form-group">
             <label htmlFor="state">State</label>
             <select value={this.state.stateId} onChange={this.stateChangeHandler} className="form-control">
                <option>Select state</option>
                {this.getSelectOptions(this.state.states)}
             </select>
          </div>

          <div className="form-group">
             <label htmlFor="state">Cities</label>
             <select value={this.state.cityId} className="form-control" onChange={event =>{this.setState({ cityId: event.target.value })}}>
                <option>Select city</option>
                {this.getSelectOptions(this.state.cities)}
             </select>
          </div>

          <div className="form-group">
            <label htmlFor="nit">Nit</label>
              <input type="text" className="form-control" id="nit" placeholder="123456789" value={this.state.nit} onChange={event => this.setState({ nit: event.target.value })} />
          </div>
          <div className="form-group">
            <label htmlFor="fullName">Full name</label>
              <input type="text" className="form-control" id="fullName" placeholder="John Doe" value={this.state.fullName} onChange={event => this.setState({ fullName: event.target.value })}   />
          </div>
          <div className="form-group">
            <label htmlFor="phone">Phone</label>
              <input type="text" className="form-control" id="phone" placeholder="5555555" value={this.state.phone} onChange={event => this.setState({ phone: event.target.value })}   />
          </div>
          <div className="form-group">
            <label htmlFor="address">Address</label>
              <input type="text" className="form-control" id="address" placeholder="Calle 100 # 100 - 100" value={this.state.address} onChange={event => this.setState({ address: event.target.value })}  />
          </div>
          <div className="form-group">
            <label htmlFor="creditLimit">Credit Limit</label>
              <input type="number" className="form-control" id="creditLimit" placeholder="10" value={this.state.creditLimit}  onChange={event => this.setState({ creditLimit: event.target.value })} />
          </div>
          <div className="form-group">
            <label htmlFor="visitsPercentage">Visit Percentage</label>
              <input type="number" className="form-control" id="visitsPercentage" placeholder="20" value={this.state.visitsPercentage} onChange={event => this.setState({ visitsPercentage: event.target.value })}  />
          </div>
              <button className="btn btn-primary">Add Client</button>
          </form>
      </div>
    	
    );
  }
}

export default EditForm

