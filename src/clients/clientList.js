import React,{useState, useEffect} from 'react';
import Client from './client';
import axios from 'axios';

const ClientList = () => {

	const deleteClient = id => {
		const resp = axios.delete(`http://localhost:8080/client/${id}`);
    	console.log("Delete client "+JSON.stringify(resp.data));
		setClients(clients.filter(client => client.id !== id))
	}

	useEffect(() => {
		fetchClients();
	},[]);

	const [clients, setClients] = useState([]);

	const fetchClients = async () => {
		const data = await fetch(
			`http://localhost:8080/clients`
		);

		const clients = await data.json();
		setClients(clients);
	};

	return(
		<div>
		  	<table className="table table-bordered">
		        <thead>
			        <tr>
			        	<th scope="col">Nit</th>
			            <th scope="col">Name</th>
			            <th scope="col">Phone</th>
			            <th scope="col">Address</th>
			            <th scope="col">Available Credit</th>
			            <th scope="col">Visits Percentage</th>
			            <th scope="col">Credit Limit</th>
			            <th scope="col"></th>
			            <th scope="col"></th>
			        </tr>
		        </thead>
		        <tbody>
		        	{clients.map(client => <Client key={client.id} deleteClient={deleteClient} className='card' {...client} />)}
		        </tbody>
		    </table>
		  </div>
	);
  
};


export default ClientList