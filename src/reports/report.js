import React,{useState, useEffect} from 'react';
import NumberVisitsByCity from './numberVisitsByCity';

const Report = () => {

	useEffect(() => {
		fetchReport();
	},[]);
	

	const [report, setReport] = useState([]);

	const fetchReport = async () => {
		const data = await fetch(
			`http://localhost:8080/visits/cities`
		);

		const report = await data.json();
		setReport(report);
	};

    return (
    	<div>
    		<NumberVisitsByCity report={report} />
		</div>
    );
  
}

export default Report

