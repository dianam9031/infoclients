import React from 'react';

class Credit extends React.Component {
  render() {
    const credit = this.props;
    return (
      <tr>
        <th scope="row">
          {credit.date}
        </th>
          <td>{credit.availableCredit}</td>
          <td>{credit.net}</td>
          <td>{credit.visitsPercentage}</td>
          <td>{credit.visitTotal}</td>
          <td>{credit.creditLimit}</td>
      </tr>
      
    );
  }
}

export default Credit

