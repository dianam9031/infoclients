import React from 'react'
import {
  BarChart, Bar, Cell, XAxis, YAxis, CartesianGrid, Tooltip, Legend,
} from 'recharts';

class NumberVisitsByCity extends React.Component {
  render() {
    const report = this.props.report;
    console.log("Esto es Report "+JSON.stringify(report));
    return (
    	<div>

	    	<h1>Number of visits by city</h1>
		    <BarChart width={600} height={300} data={report}
	            margin={{top: 5, right: 30, left: 20, bottom: 5}}>
		       <CartesianGrid strokeDasharray="3 3"/>
		       <XAxis dataKey="cityName"/>
		       <YAxis/>
		       <Tooltip/>
		       <Bar dataKey="count" fill="#82ca9d" />
		    </BarChart>
		</div>
      
    );
  }
}

export default NumberVisitsByCity





