import React,{useState, useEffect} from 'react';
import Credit from './creditDetails';

const ClientCreditHistory = ({match}) => {


	useEffect(() => {
		fetchCreditHistory();
	},[]);

	const [creditHistory, setCreditHistory] = useState([]);

	const fetchCreditHistory = async () => {
		//console.log("PARAMS "+);
		const data = await fetch(
			`http://localhost:8080/visits/credit-limit/${match.params.id}`
		);

		const creditHistory = await data.json();
		setCreditHistory(creditHistory);
	};

	return(
		<div>
		  	<table className="table table-bordered">
		        <thead>
			        <tr>
			        	<th scope="col">Date</th>
			        	<th scope="col">Available Credit</th>
			        	<th scope="col">Net</th>
			            <th scope="col">Visits Percentage</th>
			            <th scope="col">Total Visits</th>
			            <th scope="col">Credit Limit</th>
			        </tr>
		        </thead>
		        <tbody>
		        	{creditHistory.map(credit => <Credit key={credit.id} className='card' {...credit} />)}
		        </tbody>
		    </table>
		  </div>
	);
  
};


export default ClientCreditHistory